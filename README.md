wiki-search
===========
This is a simple scala service based on Typelevel stack, that can upload wiki json dumps
into PostgreSQL and gives us opportunity to search articles by title.

## Database schema
In the following schema we can see reference table categories, proxy table categories_articles,
that builds many-to-many relationship between categories and articles tables.
 ![components](./db_schema.png)

## Environment variables
`APP_ENV` - environment variable for choosing running environment (test, prod).
Choose `test` for running application locally.

`HOST` - HTTP server address

`PORT` - HTTP server port

`POSTGRES_PASSWORD` - password for PostgreSQL. Use `postgres` for local running.

## How to run
1. Use command `docker-compose up` from `Docker` folder to run database.
2. Run application with default environment variables values.
3. Use REST API and enjoy this service :)

## Endpoints

**POST** /dump
Use this endpoint for uploading dump into PosgreSQL.
`path` - absolute file path to the dump.

```
{
    "path" : "/home/yourpc/Projects/wiki-search/src/main/resources/test.json"
}
```

**GET /wiki**
Use this endpoint for searching articles by title.

It has two parameters:
1. title - this is an article title
2. pretty - if you want to get prettyfied JSON, set this parameter to `true`


Example
`http://localhost:8081/wiki?title=шахматисты армении&pretty=true`


Response:
```
  {
    "title": "Шахматисты Армении",
    "category": [
        "Шахматисты по странам",
        "Персоналии:Армения"
    ],
    "language": "ru",
    "createTimestamp": 1348113745000,
    "timestamp": 1351178362000,
    "auxiliaryText": []
  }
```
