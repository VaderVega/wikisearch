import sbt._

object Dependencies {

  object V {
    val cats             = "2.6.1"
    val catsEffect       = "3.2.9"
    val circe            = "0.14.1"
    val ciris            = "2.3.2"
    val http4s           = "0.23.1"
    val log4cats         = "2.1.1"
    val refined          = "0.9.28"
    val betterMonadicFor = "0.3.1"
    val kindProjector    = "0.13.2"
    val logback          = "1.2.6"
    val organizeImports  = "0.5.0"
    val semanticDB       = "4.4.33"
    val weaver           = "0.7.6"
    val doobie           = "1.0.0-RC1"
    val fs2           = "3.1.3"
    val fs2Json = "1.3.1"
    val scalaMock = "5.1.0"
    val scalaTest = "3.1.0"
  }

  object Libraries {
    def circe(artifact: String): ModuleID  = "io.circe"   %% s"circe-$artifact"  % V.circe
    def http4s(artifact: String): ModuleID = "org.http4s" %% s"http4s-$artifact" % V.http4s
    def ciris(artifact: String): ModuleID  = "is.cir"     %% artifact            % V.ciris

    val cats           = "org.typelevel"    %% "cats-core"      % V.cats
    val catsEffect     = "org.typelevel"    %% "cats-effect"    % V.catsEffect

    val cirisCore    = ciris("ciris")
    val cirisEnum    = ciris("ciris-enumeratum")
    val cirisRefined = ciris("ciris-refined")

    val fs2        = "co.fs2"           %% "fs2-core"    % V.fs2
    val fs2JsonCirce = "org.gnieh"      %% "fs2-data-json-circe" % V.fs2Json
    val fs2Json = "org.gnieh" %% "fs2-data-json" % V.fs2Json

    val circeCore    = circe("core")
    val circeGeneric = circe("generic")
    val circeParser  = circe("parser")
    val circeRefined = circe("refined")

    val http4sDsl    = http4s("dsl")
    val http4sServer = http4s("ember-server")
    val http4sClient = http4s("ember-client")
    val http4sCirce  = http4s("circe")

    val doobieCore   = "org.tpolecat" %% "doobie-core" % V.doobie
    val doobiePostgres = "org.tpolecat" %% "doobie-postgres" % V.doobie
    val doobieHikari = "org.tpolecat" %% "doobie-hikari" % V.doobie

    val log4cats = "org.typelevel" %% "log4cats-slf4j" % V.log4cats

    val refinedCore = "eu.timepit" %% "refined"        % V.refined
    val refinedCats = "eu.timepit" %% "refined-cats"   % V.refined

    // Runtime
    val logback = "ch.qos.logback" % "logback-classic" % V.logback

    // Test
    val log4catsNoOp      = "org.typelevel"       %% "log4cats-noop"      % V.log4cats
    val weaverCats        = "com.disneystreaming" %% "weaver-cats"        % V.weaver
    val weaverDiscipline  = "com.disneystreaming" %% "weaver-discipline"  % V.weaver
    val weaverScalaCheck  = "com.disneystreaming" %% "weaver-scalacheck"  % V.weaver
    val refinedScalacheck = "eu.timepit"          %% "refined-scalacheck" % V.refined
    val scalaMock         = "org.scalamock"       %% "scalamock"          % V.scalaMock % Test
    val scalaTest         = "org.scalatest"       %% "scalatest"          % V.scalaTest % Test

    // Scalafix rules
    val organizeImports = "com.github.liancheng" %% "organize-imports" % V.organizeImports
  }

  object CompilerPlugin {
    val betterMonadicFor = compilerPlugin(
      "com.olegpy" %% "better-monadic-for" % V.betterMonadicFor
    )
    val kindProjector = compilerPlugin(
      "org.typelevel" % "kind-projector" % V.kindProjector cross CrossVersion.full
    )
    val semanticDB = compilerPlugin(
      "org.scalameta" % "semanticdb-scalac" % V.semanticDB cross CrossVersion.full
    )
  }
}
