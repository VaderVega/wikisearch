package wikisearch

import cats.effect.{IO, IOApp}
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import wikisearch.config.Config
import wikisearch.modules.{HttpApi, Services}
import wikisearch.resources.{AppResources, MkHttpServer}

object Main extends IOApp.Simple {

  implicit val logger = Slf4jLogger.getLogger[IO]

  override def run: IO[Unit] =
    Config.load[IO].flatMap { cfg =>
      Logger[IO].info(s"Loaded config $cfg") >>
        AppResources.make[IO](cfg)
          .evalMap { res =>
            IO {
              val services = Services.make[IO](res.postgresTransactor)
              val httpApi = HttpApi.make[IO](services)
              cfg.httpServerConfig -> httpApi.httpApp
            }
        }.flatMap {
          case (cfg, httpApp) => MkHttpServer[IO].newEmber(cfg, httpApp)
        }.useForever
    }
}
