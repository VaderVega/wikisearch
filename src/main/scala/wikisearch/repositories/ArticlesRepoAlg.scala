package wikisearch.repositories

import wikisearch.domain.Article

trait ArticlesRepoAlg[F[_]] {
  def insertMany(articles: List[Article]): F[Int]
  def getArticleByTitle(title: String): F[Option[Article]]
}
