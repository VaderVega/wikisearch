package wikisearch.repositories

import cats.data.OptionT
import cats.effect.{Resource, Sync}
import cats.implicits._
import doobie.implicits._
import doobie.postgres.implicits._
import doobie.hikari.HikariTransactor
import doobie.util.update.Update
import wikisearch.domain.Category


class CategoriesRepo[F[_] : Sync] private(postgres: Resource[F, HikariTransactor[F]]) extends CategoriesRepoAlg[F] {
  def insertMany(categories: List[Category]): F[Int] =
    postgres.use { xa =>
      val sql = "insert into categories (id, category) values (? , ?)"
      Update[Category](sql).updateMany(categories)
        .transact(xa)
    }

  def getCategoriesByArticleId(articleId: Int): F[List[String]] =
     postgres.use { xa =>
      val select = fr"select c.category from categories c"
      val join = fr"join categories_articles ca on c.id = ca.category_id"
      val where = fr"where ca.article_id = $articleId"
      val query = select ++ join ++ where

      query
        .query[String]
        .to[List]
        .transact(xa)
    }
}

object CategoriesRepo {
  def make[F[_] : Sync](postgres: Resource[F, HikariTransactor[F]]) =
    new CategoriesRepo[F](postgres)
}
