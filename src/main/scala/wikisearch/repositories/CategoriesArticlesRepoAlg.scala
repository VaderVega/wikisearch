package wikisearch.repositories

import wikisearch.domain.CategoryToArticle

trait CategoriesArticlesRepoAlg[F[_]] {
  def insertMany(connections: List[CategoryToArticle]): F[Int]
}
