package wikisearch.repositories

import cats.data.OptionT
import cats.effect.{Resource, Sync}
import doobie.hikari.HikariTransactor
import doobie.util.update.Update
import doobie.implicits._
import doobie.implicits.javasql._
import doobie.postgres.implicits._
import wikisearch.domain.Article

class ArticleRepo[F[_] : Sync] private(postgres: Resource[F, HikariTransactor[F]]) extends ArticlesRepoAlg[F] {
  def insertMany(articles: List[Article]): F[Int] = {
    postgres.use { xa =>
      val sql = "insert into articles (article_id, title, language, create_timestamp, update_timestamp, auxiliary_text)" +
        "values (?, ?, ?, ?, ?, ?)"
      Update[Article](sql).updateMany(articles)
        .transact(xa)
    }
  }

  def getArticleByTitle(title: String): F[Option[Article]] =
    postgres.use { xa =>
      val sql = sql"select * from articles where title = $title"

      sql
        .query[Article]
        .option
        .transact(xa)
    }
}

object ArticleRepo {
  def make[F[_] : Sync](postgres: Resource[F, HikariTransactor[F]]): ArticleRepo[F] =
    new ArticleRepo[F](postgres) { }
}
