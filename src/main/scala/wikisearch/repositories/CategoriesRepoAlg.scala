package wikisearch.repositories

import wikisearch.domain.Category


trait CategoriesRepoAlg[F[_]] {
  def insertMany(categories: List[Category]): F[Int]
  def getCategoriesByArticleId(articleId: Int): F[List[String]]
}
