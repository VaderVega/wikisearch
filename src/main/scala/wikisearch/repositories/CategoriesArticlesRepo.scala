package wikisearch.repositories

import cats.effect.{Resource, Sync}
import doobie.hikari.HikariTransactor
import doobie.util.update.Update
import doobie.implicits._
import wikisearch.domain.CategoryToArticle

class CategoriesArticlesRepo [F[_] : Sync] private(postgres: Resource[F, HikariTransactor[F]]) extends CategoriesArticlesRepoAlg[F] {
  override def insertMany(connections: List[CategoryToArticle]): F[Int] =
    postgres.use { xa =>
      val sql = "insert into categories_articles (category_id, article_id) values (?, ?)"
      Update[CategoryToArticle](sql).updateMany(connections)
        .transact(xa)
    }
}


object CategoriesArticlesRepo {
  def make[F[_] : Sync](postgres: Resource[F, HikariTransactor[F]]): CategoriesArticlesRepo[F] =
    new CategoriesArticlesRepo[F](postgres) { }
}
