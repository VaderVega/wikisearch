package wikisearch.services

import wikisearch.domain.ArticleResponse

trait ArticleSearchServiceAlg[F[_]] {
  def findArticleByTitle(title: String): F[Option[ArticleResponse]]
}
