package wikisearch.services

import cats.effect.Sync
import cats.implicits._
import org.typelevel.log4cats.Logger
import wikisearch.domain.ArticleResponse
import wikisearch.repositories.{ArticlesRepoAlg, CategoriesRepoAlg}

class ArticleSearchService[F[_] : Sync : Logger] private(categoriesRepo: CategoriesRepoAlg[F],
                                                         articlesRepo: ArticlesRepoAlg[F])
  extends ArticleSearchServiceAlg[F] {

  def findArticleByTitle(title: String): F[Option[ArticleResponse]] = {
    val capitalizedTitle = capitalizeTitle(title)

    for {
      a <- articlesRepo.getArticleByTitle(capitalizedTitle)
      categories <- a match {
        case Some(article) => categoriesRepo.getCategoriesByArticleId(article.articleId)
        case None => List.empty[String].pure[F]
      }
    } yield a match {
      case Some(article) =>
        ArticleResponse(article.title,
          categories,
          article.language,
          article.createTimestamp.getTime,
          article.timestamp.getTime,
          article.auxiliaryText).some
      case None => None
    }
  }

  private[services] def capitalizeTitle(title: String): String =
    title
      .split(" ")
      .toList
      .map(_.capitalize)
      .mkString(" ")

}

object ArticleSearchService {
  def make[F[_] : Sync : Logger](categoriesRepo: CategoriesRepoAlg[F],
                                 articlesRepo: ArticlesRepoAlg[F]): ArticleSearchService[F] =
    new ArticleSearchService[F](categoriesRepo: CategoriesRepoAlg[F], articlesRepo: ArticlesRepoAlg[F])
}
