package wikisearch.services

import cats.implicits._
import cats.effect.Sync
import org.typelevel.log4cats.Logger
import wikisearch.domain.{Article, Category, CategoryToArticle, DumpData}
import wikisearch.repositories.{ArticlesRepoAlg, CategoriesArticlesRepoAlg, CategoriesRepoAlg}

import java.sql.Timestamp
import java.text.{DateFormat, SimpleDateFormat}
import scala.annotation.tailrec

class DatabaseInitService[F[_] : Sync : Logger] private(dumpDownloadService: DumpDownloadAlg[F],
                                               categoriesRepo: CategoriesRepoAlg[F],
                                               articlesRepo: ArticlesRepoAlg[F],
                                               categoriesArticlesRepo: CategoriesArticlesRepoAlg[F]) extends DatabaseInitAlg[F] {

  def uploadDumpToDBFromPath(path: String): F[String] = {
    implicit val dateFormat: DateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

    for {
      dumpData <- dumpDownloadService.downloadDump(path)
      _ <- Logger[F].info(s"dump from path $path was read")
      categories <- Sync[F].delay(extractCategoryList(dumpData))
      _ <- categoriesRepo.insertMany(categories)
      _ <- Logger[F].info("categories were uploaded to DB")
      articles <- Sync[F].delay(extractArticles(dumpData))
      _ <- articlesRepo.insertMany(articles)
      _ <- Logger[F].info("articles were uploaded to DB")
      categoriesMap = createCategoriesToIdsMap(categories)
      articlesMap = createArticlesToIdsMap(articles)
      categoryToArticles = formCategoryToArticleConnections(dumpData, categoriesMap, articlesMap)
      _ = categoryToArticles.foreach(println)
      _ <- categoriesArticlesRepo.insertMany(categoryToArticles)
      _ <- Logger[F].info("category-to-article connections were uploaded to DB")
    } yield "Ok"
  }

  private[services] def extractCategoryList(dump: List[DumpData]): List[Category] = {
    @tailrec
    def createCategoryTailrec(categories: List[String], acc: List[Category], id: Int): List[Category] =
      categories match {
        case Nil => acc
        case h :: Nil => acc ::: List(Category(id, h))
        case h :: t => createCategoryTailrec(t, acc ::: List(Category(id, h)), id + 1)
      }

    val unprocessedCategories = dump.flatMap(d => d.category).distinct
    createCategoryTailrec(unprocessedCategories, List.empty[Category], 1)
  }

  private[services] def extractArticles(dump: List[DumpData])(implicit dateFormat: DateFormat): List[Article] = {
    @tailrec
    def createArticleTailrec(dump: List[DumpData], acc: List[Article], id: Int): List[Article] =
      dump match {
        case Nil => acc
        case h :: Nil => acc ::: List(Article(id, h.title, h.language,
          new Timestamp(dateFormat.parse(h.create_timestamp).getTime), new Timestamp(dateFormat.parse(h.timestamp).getTime), h.auxiliary_text))
        case h :: t => createArticleTailrec(t, acc ::: List(Article(id, h.title, h.language,
          new Timestamp(dateFormat.parse(h.create_timestamp).getTime), new Timestamp(dateFormat.parse(h.timestamp).getTime), h.auxiliary_text)), id + 1)
      }

    createArticleTailrec(dump, List.empty[Article], 1)
  }

  private[services] def createCategoriesToIdsMap(categoriesList: List[Category]): Map[String, Int] =
    categoriesList.map(c => c.name -> c.id).toMap

  private[services] def createArticlesToIdsMap(articlesList: List[Article]): Map[String, Int] =
    articlesList.map(a => a.title -> a.articleId).toMap

  private[services] def formCategoryToArticleConnections(dump: List[DumpData],
                                                         categoriesToIdsMap: Map[String, Int],
                                                         articlesToIdsMap: Map[String, Int]): List[CategoryToArticle] =
    dump.flatMap { d =>
      val categoryIds = d.category.map(categoriesToIdsMap(_))
      val articleId = articlesToIdsMap(d.title)
      categoryIds.map(id => CategoryToArticle(id, articleId))
    }
}


object DatabaseInitService {
  def make[F[_] : Sync : Logger](dumpDownloadService: DumpDownloadAlg[F],
                        categoriesRepo: CategoriesRepoAlg[F],
                        articlesRepo: ArticlesRepoAlg[F],
                        categoriesArticlesRepo: CategoriesArticlesRepoAlg[F]): DatabaseInitService[F] =
    new DatabaseInitService[F](dumpDownloadService, categoriesRepo, articlesRepo, categoriesArticlesRepo)
}
