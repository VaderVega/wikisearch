package wikisearch.services

import cats.implicits._
import cats.effect.{Sync, Async}
import fs2.RaiseThrowable
import fs2.data.json.circe.CirceBuilder
import fs2.data.json.selector.root
import fs2.data.json.{ast, filter, tokens}
import fs2.io.file.{Files, Path}
import fs2.text.utf8
import io.circe.Json
import wikisearch.domain.DumpData

class DumpDownloadService[F[_] : Async : RaiseThrowable] private extends DumpDownloadAlg[F] {
  def downloadDump(path: String): F[List[DumpData]] = {
    val sel = root.fields(
      "auxiliary_text",
      "language",
      "title",
      "create_timestamp",
      "category",
      "timestamp",
    ).compile

    val streamEffect = Sync[F].blocking {
      Files[F]
        .readAll(Path(path))
        .through(utf8.decode)
        .through(tokens[F, String])
        .through(filter(sel, wrap = true))
        .through(ast.values[F, Json])
        .map(_.as[DumpData])
        .filter(_.isRight)
        .map(x => x.toTry.get)
    }

    for {
      stream <- streamEffect
      uploadingData <- stream.compile.toList
    } yield uploadingData
  }
}

object DumpDownloadService {
  def make[F[_] : Async : RaiseThrowable] = new DumpDownloadService[F]
}
