package wikisearch.services

import wikisearch.domain.DumpData

trait DumpDownloadAlg[F[_]] { // todo implement it
  def downloadDump(path: String): F[List[DumpData]]
}
