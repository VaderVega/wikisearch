package wikisearch.services


trait DatabaseInitAlg[F[_]] {
  def uploadDumpToDBFromPath(path: String): F[String]
}
