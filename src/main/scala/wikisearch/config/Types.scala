package wikisearch.config

import ciris._
import ciris.refined._
import com.comcast.ip4s.{Host, Port}
import eu.timepit.refined.types.numeric.PosInt
import eu.timepit.refined.types.net.UserPortNumber
import eu.timepit.refined.types.string.NonEmptyString

object Types {
  case class HttpServerConfig(host: Host, port: Port)

  case class PostgreSQLConfig(
                               host: NonEmptyString,
                               port: UserPortNumber,
                               user: NonEmptyString,
                               password: Secret[NonEmptyString],
                               database: NonEmptyString,
                               max: PosInt
                             )

  case class AppConfig(
                        postgreSQL: PostgreSQLConfig,
                        httpServerConfig: HttpServerConfig
                      )
}
