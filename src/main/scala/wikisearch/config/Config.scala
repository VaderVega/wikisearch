package wikisearch.config

import cats.effect.Async
import cats.syntax.all._
import ciris._
import ciris.refined._
import com.comcast.ip4s._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.types.string.NonEmptyString
import wikisearch.config.AppEnvironment._
import wikisearch.config.Types.{AppConfig, HttpServerConfig, PostgreSQLConfig}

object Config {
  def load[F[_] : Async]: F[AppConfig] = {
    env("APP_ENV")
      .as[AppEnvironment]
      .flatMap {
        case Test =>
          default[F](
            postgresHost = "localhost",
            postgresUser = "postgres"
          )
        case Prod =>
          default[F](
            postgresHost = "localhost",
            postgresUser = "postgres"
          )
      }.load[F]
  }

  private def default[F[_]](postgresHost: NonEmptyString,
                            postgresUser: NonEmptyString
                           ): ConfigValue[F, AppConfig] =
    (
      env("HOST").as[NonEmptyString],
      env("PORT").as[Int],
      env("POSTGRES_PASSWORD").as[NonEmptyString].secret
    ).parMapN { (h, p, postgresPass) =>
      val host: Host = Host.fromString(h).getOrElse(host"0.0.0.0")
      val port: Port = Port.fromInt(p).getOrElse(port"8080")
      AppConfig(
        PostgreSQLConfig(
          host = postgresHost,
          port = 5432,
          user = postgresUser,
          password = postgresPass,
          database = "wikisearch",
          max = 10
        ),
        HttpServerConfig(
          host = host,
          port = port
        )
      )
    }

}
