package wikisearch.http.routes

import cats.Monad
import cats.effect.Concurrent
import cats.implicits._
import io.circe.syntax._
import org.http4s.{HttpRoutes, Response}
import org.http4s.dsl.Http4sDsl
import wikisearch.services.ArticleSearchServiceAlg

final case class WikiRoutes[F[_] : Monad : Concurrent](articleSearchService: ArticleSearchServiceAlg[F]) extends Http4sDsl[F] {

  object OptionalPrettyQueryParamMatcher extends OptionalQueryParamDecoderMatcher[Boolean]("pretty")

  object TitleQueryParamMatcher extends QueryParamDecoderMatcher[String]("title")

  val routes: HttpRoutes[F] = HttpRoutes.of[F] {
    case GET -> Root / "wiki" :? TitleQueryParamMatcher(title) +& OptionalPrettyQueryParamMatcher(maybePretty) =>
      (title, maybePretty) match {
        case (title, Some(isPretty)) =>
          formArticleSearchResponse(title, isPretty)
        case (title, None) =>
          formArticleSearchResponse(title, false)
        case _ => BadRequest("param title must be specified")
      }

  }

  private def formArticleSearchResponse(title: String, isPretty: Boolean): F[Response[F]] =
    for {
      maybeArticle <- articleSearchService.findArticleByTitle(title)
      answer <- maybeArticle match {
        case Some(article) =>
          if (isPretty) Ok(article)
          else Ok(article.asJson.toString().replace("\n", ""))
        case None => NotFound(s"Article with title $title not found")
      }
    } yield answer

}


