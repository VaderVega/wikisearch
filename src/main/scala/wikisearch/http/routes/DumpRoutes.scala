package wikisearch.http.routes

import cats.{Monad, MonadThrow}
import cats.effect.Concurrent
import cats.implicits._
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import wikisearch.domain.DumpUploadRequest
import wikisearch.services.DatabaseInitAlg

final case class DumpRoutes[F[_] : Concurrent : Monad : MonadThrow ](dbInitService: DatabaseInitAlg[F]) extends Http4sDsl[F] {
  val routes: HttpRoutes[F] = HttpRoutes.of[F] {
    case req @ POST -> Root / "dump"  =>
        req.as[DumpUploadRequest].flatMap { r =>
          val resp = for {
            resp <- dbInitService.uploadDumpToDBFromPath(r.path)
          } yield resp
          Ok(resp)
      }
  }
}
