package wikisearch.resources

import cats.effect.std.Console
import cats.effect.{Async, Resource}
import cats.syntax.all._
import doobie.hikari.HikariTransactor
import doobie.implicits._
import doobie.util.ExecutionContexts
import eu.timepit.refined.auto._
import org.typelevel.log4cats.Logger
import wikisearch.config.Types.{AppConfig, PostgreSQLConfig}

sealed abstract class AppResources[F[_]](val postgresTransactor: Resource[F, HikariTransactor[F]])

object AppResources {

  def make[F[_] : Async : Console : Logger](cfg: AppConfig): Resource[F, AppResources[F]] = {

    def checkPostgresConnection(transactor: HikariTransactor[F]): F[Unit] =
      sql"select version();"
        .query
        .unique
        .transact(transactor)
        .flatMap { v =>
          Logger[F].info(s"Connected to Postgres $v")
        }

    def mkPostgreSqlResource(c: PostgreSQLConfig): Resource[F, HikariTransactor[F]] =
      (for {
        ec <- ExecutionContexts.fixedThreadPool[F](32)
        xa <- HikariTransactor.newHikariTransactor[F](
          "org.postgresql.Driver",
          s"jdbc:postgresql:${c.database}",
          c.user,
          c.password.value,
          ec
        )
      } yield xa)
        .evalTap(checkPostgresConnection)


    Resource.eval(new AppResources[F](mkPostgreSqlResource(cfg.postgreSQL)) {}.pure[F])
  }

}