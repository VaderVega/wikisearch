package wikisearch.modules

import cats.effect.{Async, Resource, Sync, Temporal}
import doobie.hikari.HikariTransactor
import org.typelevel.log4cats.Logger
import wikisearch.repositories.{ArticleRepo, CategoriesArticlesRepo, CategoriesRepo}
import wikisearch.services.{ArticleSearchService, ArticleSearchServiceAlg, DatabaseInitAlg, DatabaseInitService, DumpDownloadAlg, DumpDownloadService}

sealed abstract class Services[F[_]] private(val dumpDownloadService: DumpDownloadAlg[F],
                                             val databaseInitService: DatabaseInitAlg[F],
                                             val articleSearchService: ArticleSearchServiceAlg[F])

object Services {
  def make[F[_] : Temporal : Async : Logger](postgres: Resource[F, HikariTransactor[F]]): Services[F] = {
    val categoriesRepo = CategoriesRepo.make(postgres)
    val articleRepo = ArticleRepo.make(postgres)
    val categoriesArticlesRepo = CategoriesArticlesRepo.make(postgres)

    val dumpDownloadService = DumpDownloadService.make
    val databaseInitService = DatabaseInitService.make(dumpDownloadService, categoriesRepo, articleRepo, categoriesArticlesRepo)
    val articleSearchService = ArticleSearchService.make(categoriesRepo, articleRepo)

    new Services[F](dumpDownloadService, databaseInitService, articleSearchService) {}
  }
}
