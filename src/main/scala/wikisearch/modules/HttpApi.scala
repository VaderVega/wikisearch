package wikisearch.modules

import cats.Parallel
import cats.effect.Async
import cats.implicits._
import org.http4s.{HttpApp, HttpRoutes}
import org.http4s.implicits._
import org.http4s.server.middleware._
import wikisearch.http.routes.{DumpRoutes, WikiRoutes}

import scala.concurrent.duration._

sealed abstract class HttpApi[F[_] : Async : Parallel] private(services: Services[F]) {
  private val dumpRoutes = DumpRoutes[F](services.databaseInitService).routes
  private val wikiRoutes = WikiRoutes[F](services.articleSearchService).routes

  private val middleware: HttpRoutes[F] => HttpRoutes[F] = {
    { http: HttpRoutes[F] =>
      AutoSlash(http)
    } andThen { http: HttpRoutes[F] =>
      Timeout(60.seconds)(http)
    }
  }

  private val loggers: HttpApp[F] => HttpApp[F] = {
    {
      http: HttpApp[F] =>
        RequestLogger.httpApp(true, true)(http)
    } andThen { http: HttpApp[F] =>
      ResponseLogger.httpApp(true, true)(http)
    }
  }

  private val routes: HttpRoutes[F] = dumpRoutes <+> wikiRoutes

  val httpApp: HttpApp[F] = loggers(middleware(routes).orNotFound)
}

object HttpApi {
  def make[F[_] : Async : Parallel](services: Services[F]): HttpApi[F] =
    new HttpApi[F](services) {}
}
