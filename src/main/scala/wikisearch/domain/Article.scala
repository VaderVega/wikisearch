package wikisearch.domain


import java.sql.Timestamp

case class Article(articleId: Int,
                   title: String,
                   language: String,
                   createTimestamp: Timestamp,
                   timestamp: Timestamp,
                   auxiliaryText: List[String])
