package wikisearch.domain

import cats.effect.Concurrent
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder
import org.http4s.EntityEncoder
import org.http4s.circe.jsonEncoderOf

case class ArticleResponse(title: String,
                           category: List[String],
                           language: String,
                           createTimestamp: Long,
                           timestamp: Long,
                           auxiliaryText: List[String])

object ArticleResponse {

  implicit val articleResponseEncoder: Encoder[ArticleResponse] =
    deriveEncoder[ArticleResponse]

  implicit def articleResponseEntityEncoder[F[_] : Concurrent]: EntityEncoder[F, ArticleResponse] =
    jsonEncoderOf[F, ArticleResponse]

}
