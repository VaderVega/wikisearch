package wikisearch.domain


case class Category(id: Int, name: String)
