package wikisearch.domain

import cats.effect.Concurrent
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import org.http4s.EntityDecoder
import org.http4s.circe.jsonOf

case class DumpUploadRequest(path: String)

object DumpUploadRequest {

  implicit val dumpUploadRequestDecoder: Decoder[DumpUploadRequest] =
    deriveDecoder[DumpUploadRequest]

  implicit def dumpUploadRequestEntityDecoder[F[_] : Concurrent]: EntityDecoder[F, DumpUploadRequest] =
    jsonOf[F, DumpUploadRequest]

}
