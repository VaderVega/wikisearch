package wikisearch.domain

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class DumpData(
                     auxiliary_text: List[String],
                     language: String,
                     title: String,
                     create_timestamp: String,
                     category: List[String],
                     timestamp: String,
                   )

object DumpData {

  implicit val dumpDataDecoder: Decoder[DumpData] = deriveDecoder[DumpData]

}
