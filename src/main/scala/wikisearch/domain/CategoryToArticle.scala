package wikisearch.domain

case class CategoryToArticle(categoryId: Int,
                             articleId: Int)
