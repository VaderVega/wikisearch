package wikisearch.generators

import org.scalacheck.Gen

object Generators {
  val pathGen: Gen[String] = Gen.oneOf(Seq("src/test/resources/test.json"))
}
