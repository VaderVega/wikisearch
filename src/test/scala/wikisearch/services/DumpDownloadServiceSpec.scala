package wikisearch.services

import cats.effect.IO
import weaver.SimpleIOSuite
import weaver.scalacheck.Checkers
import wikisearch.generators.Generators

object DumpDownloadServiceSpec extends SimpleIOSuite with Checkers {
  val gen = Generators.pathGen

  test("json dump unmarshalling works well") {
    val dumpDownloadService = DumpDownloadService.make[IO]

    forall(gen) { p =>
      dumpDownloadService.downloadDump(p)
        .attempt
        .map {
          case Right(_) => success
          case _ => failure("Perhaps there is a problem with file path")
        }
    }
  }
}
