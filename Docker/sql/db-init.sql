CREATE DATABASE wikisearch;
\c wikisearch;

CREATE TABLE IF NOT EXISTS categories (
    id SERIAL NOT NULL PRIMARY KEY,
    category VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS articles (
    article_id SERIAL PRIMARY KEY,
    title CHARACTER VARYING NOT NULL,
    language CHAR(2) NOT NULL,
    create_timestamp TIMESTAMP NOT NULL,
    update_timestamp TIMESTAMP NOT NULL,
    auxiliary_text VARCHAR[]
);

CREATE TABLE IF NOT EXISTS categories_articles (
    id SERIAL PRIMARY KEY,
    category_id INTEGER NOT NULL CONSTRAINT fk_category_id REFERENCES categories,
    article_id INTEGER NOT NULL CONSTRAINT fk_article_id REFERENCES articles ON DELETE CASCADE
);

CREATE INDEX title_index ON articles (title);

COMMIT;
